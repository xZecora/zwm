# zwm: my attempt at a tiling WM

- tiles
- floating windows
- workspaces
- patches
- Multi-Monitor (more or less)

If you want something more, add it! If you want some simple functionality, you can probably make it work. I also have patches for it over at [zwm patches](https://gitlab.com/xZecora/zwm-patches) (currently on hold for the time being), so maybe what you want is there.

# TODO
:heavy_check_mark: add multi-monitor functionality \
:heavy_check_mark: make things more patch oriented \
add more comments to make it readable as a resource \
thinking about changing hotkeys to function more like ratpoison \
add more tiling layouts \
considering making this a multi file project for readability sake

# sources
A lot of inspiration and code taken from dwm and qpwm
